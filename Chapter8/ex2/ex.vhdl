-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity ex is
  port
    ( X1, X2     : in  std_logic
    ; RESET, CLK : in  std_logic
    ; Z          : out std_logic
    );
end ex;

-- architecture
architecture ex of ex is
  signal PS, NS : std_logic_vector(1 downto 0) := "10";
begin
  sync_proc: process(CLK, NS, RESET)
  begin
    if (RESET = '0') then PS <= "01";
    elsif (rising_edge(CLK)) then PS <= NS;
    end if;
  end process sync_proc;

  comb_proc: process(PS,X1,X2)
  begin
    case PS is
      when "01" =>
        if (X2='0') then
          NS<="10";
          Z<='1';
        elsif (X2='1') then
          NS <= "11";
          Z <= '0';
        else
          NS <= "XX";
          Z <= 'X';
        end if;

      when "11" =>
        if (X2='1') then
          NS<="11";
          Z<='0';
        elsif (X2='0') then
          NS <= "10";
          Z <= '1';
        else
          NS <= "XX";
          Z <= 'X';
        end if;

      when "10" =>
        if (X1='0') then
          NS<="10";
          Z<='0';
        elsif (X1='1') then
          NS <= "01";
          Z <= '0';
        else
          NS <= "XX";
          Z <= 'X';
        end if;

      when others =>
        Z <= 'X';
        NS <= "XX";
    end case;
  end process comb_proc;
end ex;
