-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity ex is
  port
    ( A, B   : in  std_logic_vector(7 downto 0)
    ; LDA    : in  std_logic
    ; SEL    : in  std_logic
    ; CLK    : in  std_logic
    ; F      : out std_logic_vector(7 downto 0)
    );
end ex;

architecture ex_a of ex is
  signal aOrB : std_logic_vector(7 downto 0);

  component mux21
    port
      ( A, B : in  std_logic_vector(7 downto 0)
      ; SEL  : in  std_logic
      ; O    : out std_logic_vector(7 downto 0)
      );
  end component;

begin
  mux21i : mux21 port map (A, B, SEL, aOrB);

  reg: process(CLK)
  begin
    if (rising_edge(CLK)) then
      if (LDA = '1') then
        F <= aOrB;
      end if;
    end if;
  end process;

end ex_a;
