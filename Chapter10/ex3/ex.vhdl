-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity ex is
  port
    ( X, Y     : in  std_logic_vector(7 downto 0)
    ; LDA, LDB : in  std_logic
    ; S0, S1   : in  std_logic
    ; CLK      : in  std_logic
    ; RB       : out std_logic_vector(7 downto 0)
    );
end ex;

architecture ex_a of ex is
  signal X_or_RB, Y_or_RA, raOut, rbOut : std_logic_vector(7 downto 0);

  component mux21
    port
      ( A, B : in  std_logic_vector(7 downto 0)
      ; SEL  : in  std_logic
      ; O    : out std_logic_vector(7 downto 0)
      );
  end component;

begin
  mux21a : mux21 port map (X, rbOut, S1, X_or_RB);

  ra_reg: process(CLK)
  begin
    if (rising_edge(CLK)) then
      if (LDA = '1') then
        raOut <= X_or_RB;
      end if;
    end if;
  end process;

  mux21b : mux21 port map (raOut, Y, S0, Y_or_RA);

  rb_reg: process(CLK)
  begin
    if (rising_edge(CLK)) then
      if (LDB = '1') then
        rbOut <= Y_or_RA;
      end if;
    end if;
  end process;

  RB <= rbOut;
end ex_a;
