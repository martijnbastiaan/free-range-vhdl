-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity ex is
  port
    ( A, B, C : in  std_logic_vector(7 downto 0)
    ; DS      : in  std_logic
    ; MS      : in  std_logic_vector(1 downto 0)
    ; CLK     : in  std_logic
    ; RA, RB  : out std_logic_vector(7 downto 0)
    );
end ex;

architecture ex_a of ex is
  signal ABC_or_RbOut, raOut, rbOut : std_logic_vector(7 downto 0);

  component mux41
    port
      ( A, B, C, D : in  std_logic_vector(7 downto 0)
      ; SEL        : in  std_logic_vector(1 downto 0)
      ; O          : out std_logic_vector(7 downto 0)
      );
  end component;

begin
  mux41i : mux41 port map (A, B, C, rbOut, MS, ABC_or_RbOut);

  ra_reg: process(CLK)
  begin
    if (falling_edge(CLK)) then
      if (DS = '0') then
        raOut <= ABC_or_RbOut;
      end if;
    end if;
  end process;

  rb_reg: process(CLK)
  begin
    if (falling_edge(CLK)) then
      if (DS = '1') then
        rbOut <= raOut;
      end if;
    end if;
  end process;

  RA <= raOut;
  RB <= rbOut;
end ex_a;
