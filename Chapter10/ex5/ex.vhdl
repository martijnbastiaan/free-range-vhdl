-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity ex is
  port
    ( A, B, C  : in  std_logic_vector(7 downto 0)
    ; SL1, SL2 : in  std_logic
    ; CLK      : in  std_logic
    ; RAX, RBX : out std_logic_vector(7 downto 0)
    );
end ex;

architecture ex_a of ex is
  signal B_or_C : std_logic_vector(7 downto 0);

  component mux21
    port
      ( A, B : in  std_logic_vector(7 downto 0)
      ; SEL  : in  std_logic
      ; O    : out std_logic_vector(7 downto 0)
      );
  end component;

begin
  mux21a : mux21 port map (B, C, SL2, B_or_C);

  ra_reg: process(CLK)
  begin
    if (rising_edge(CLK)) then
      if (SL1 = '1') then
        RAX <= A;
      end if;
    end if;
  end process;

  rb_reg: process(CLK)
  begin
    if (rising_edge(CLK)) then
      if (SL1 = '0') then
        RBX <= B_or_C;
      end if;
    end if;
  end process;
end ex_a;
