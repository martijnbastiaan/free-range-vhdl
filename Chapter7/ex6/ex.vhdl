library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port
    ( CLK     : in  std_logic
    ; S, T, R : in  std_logic
    ; Q, nQ   : out std_logic
    );
end ex;

architecture ex_1 of ex is
  signal iq : std_logic;
begin
  proc1: process(CLK, S, R)
  begin
    if    (S = '1' and R = '1')        then iq <= 'X';
    elsif (S = '1')                    then iq <= '0';
    elsif (R = '1')                    then iq <= '1';
    elsif (rising_edge(CLK) and T='1') then iq <= not iq;
    end if;
  end process proc1;

  Q <= iq;
  nQ <= not iq;
end ex_1;
