library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port
    ( CLK     : in  std_logic
    ; S, D, R : in  std_logic
    ; Q, nQ   : out std_logic
    );
end ex;

architecture ex_1 of ex is
  signal iq : std_logic;
begin
  proc1: process(CLK, S, R)
  begin
    if    (S = '0' and R = '0') then iq <= 'X';
    elsif (S = '0')             then iq <= '0';
    elsif (R = '0')             then iq <= '1';
    elsif (rising_edge(CLK))    then iq <= D;
    end if;

  end process proc1;

  Q <= iq;
  nQ <= not iq;
end ex_1;
