library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port
    ( CLK     : in  std_logic
    ; S, D, R : in  std_logic
    ; Q, nQ   : out std_logic
    );
end ex;

architecture ex_1 of ex is
  signal iq_0, iq_1 : std_logic;
begin
  proc1: process(CLK, S, R)
    variable toggle : std_logic := '0';
  begin
    if    (S = '0' and R = '0') then toggle := '1';
    elsif (S = '0')             then iq_0 <= '0';
    elsif (R = '0')             then iq_0 <= '1';
    elsif (rising_edge(CLK))    then iq_0 <= D;
    end if;

    iq_1 <= iq_0 XOR toggle;
  end process proc1;

  Q <= iq_1;
  nQ <= not iq_1;
end ex_1;
