library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B,C : in  std_logic;
         O     : out std_logic);
end ex;

architecture ex_1 of ex is
  signal I1, I2, I3, I4 : std_logic;
begin
  I1 <= (not a) and (not b) and c;
  I2 <= a and (not b) and c;
  I3 <= a and (not b) and (not c);
  I4 <= (not a) and b and c;
  O <= I1 and I2 and I3 and I4;
end ex_1;
