library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( D7,D6,D5,D4,D3,D2,D1,D0 : in  std_logic
       ; SEL                     : std_logic_vector(2 downto 0)
       ; O                       : out std_logic);
end ex;

architecture ex_1 of ex is
begin
    with SEL select
      O <=
        D0 when "000",
        D1 when "001",
        D2 when "010",
        D3 when "011",
        D4 when "100",
        D5 when "101",
        D6 when "110",
        D7 when "111",
        '0' when others;
end ex_1;
