library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( a0,a1,a2,a3,a4,a5,a6,a7 : in  std_logic;
         O                       : out std_logic);
end ex;

architecture ex_1 of ex is
begin
  O <= a0 or a1 or a2 or a3 or a4 or a5 or a6 or a7;
end ex_1;
