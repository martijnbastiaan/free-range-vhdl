library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B : in  std_logic;
         O   : out std_logic);
end ex;

architecture ex_1 of ex is
  signal I1, I2, I3 :std_logic;
begin
  I1 <= (not a) and b;
  I2 <= A;
  I3 <= A and (not b);
  O <= I1 or I2 or I3;
end ex_1;
