library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( as : in  std_logic_vector(7 downto 0)
       ; O  : out std_logic);
end ex;

architecture ex_1 of ex is
begin
  O <= '1' when (as = "11111111") else '0';
end ex_1;
