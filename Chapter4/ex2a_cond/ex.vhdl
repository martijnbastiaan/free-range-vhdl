library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B,C,D : in  std_logic;
         O       : out std_logic);
end ex;

architecture ex_1 of ex is
begin
  O <= '1' when (a = '0' and c = '1' and d = '0') else
       '1' when (b = '0' and c = '1'            ) else
       '1' when (b = '1' and c = '1' and d = '0') else
       '0';
end ex_1;
