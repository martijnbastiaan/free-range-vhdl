library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( O   : out std_logic_vector(7 downto 0)
       ; SEL : in  std_logic_vector(2 downto 0)
       );
end ex;

architecture ex_1 of ex is
begin
    with SEL select
      O <=
        "00000001" when "000",
        "00000010" when "001",
        "00000100" when "010",
        "00001000" when "011",
        "00010000" when "100",
        "00100000" when "101",
        "01000000" when "110",
        "10000000" when "111",
        "00000000" when others;
end ex_1;
