library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( D7,D6,D5,D4,D3,D2,D1,D0 : in  std_logic
       ; SEL                     : std_logic_vector(2 downto 0)
       ; O                       : out std_logic);
end ex;

architecture ex_1 of ex is
begin
  O <=
    D0 when (SEL = "000") else
    D1 when (SEL = "001") else
    D2 when (SEL = "010") else
    D3 when (SEL = "011") else
    D4 when (SEL = "100") else
    D5 when (SEL = "101") else
    D6 when (SEL = "110") else
    D7 when (SEL = "111") else
    '0';
end ex_1;
