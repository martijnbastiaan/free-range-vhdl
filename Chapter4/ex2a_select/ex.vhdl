library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B,C,D : in  std_logic;
         O       : out std_logic);
end ex;

architecture ex_1 of ex is
  signal t1 : std_logic_vector(3 downto 0);
begin
  t1 <= a & b & c & d;

  with (t1) select
    O <= '1' when "0010"|"0110",
         '1' when "0011"|"1010"|"1011", -- "0010"
         '1' when "1110",               -- "0110"
         '0' when others;
end ex_1;
