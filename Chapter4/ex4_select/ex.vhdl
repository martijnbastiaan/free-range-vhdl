library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( as : in  std_logic_vector(7 downto 0)
       ; O  : out std_logic);
end ex;

architecture ex_1 of ex is
begin
  with as select
    O <= '0' when "11111111",
         '1' when others;
end ex_1;
