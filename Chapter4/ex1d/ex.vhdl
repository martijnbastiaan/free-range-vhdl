library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B,C : in  std_logic;
         O     : out std_logic);
end ex;

architecture ex_1 of ex is
  signal I1 :std_logic;
begin
  I1 <= (not a) and (not b);
  O <= I1 and c;
end ex_1;
