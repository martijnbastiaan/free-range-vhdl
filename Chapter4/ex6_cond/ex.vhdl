library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( O   : out std_logic_vector(7 downto 0)
       ; SEL : in  std_logic_vector(2 downto 0)
       );
end ex;

architecture ex_1 of ex is
begin
  O <=
    "00000001" when (SEL = "000") else
    "00000010" when (SEL = "001") else
    "00000100" when (SEL = "010") else
    "00001000" when (SEL = "011") else
    "00010000" when (SEL = "100") else
    "00100000" when (SEL = "101") else
    "01000000" when (SEL = "110") else
    "10000000" when (SEL = "111") else
    "00000000";
end ex_1;
