library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B,C,D : in  std_logic;
         O       : out std_logic);
end ex;

architecture ex_1 of ex is
  signal I1, I2, I3 :std_logic;
begin
  I1 <= (not a) or b;
  I2 <= (not b) or c or (not d);
  I3 <= (not a) or d;
  O <= I1 and I2 and I3;
end ex_1;
