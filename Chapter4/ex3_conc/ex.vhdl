library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( a0,a1,a2,a3,a4,a5,a6,a7 : in  std_logic;
         O                       : out std_logic);
end ex;

architecture ex_1 of ex is
begin
  O <= a0 and a1 and a2 and a3 and a4 and a5 and a6 and a7;
end ex_1;
