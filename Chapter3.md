# 3.6

## 1
Product types such as `std_logic_vector`.

## 2
Forward slash.

## 3
It helps understanding.

## 4a
```vhdl
entity sys1 is
port (
  a_in1, a_in2 : in  std_logic;
  clk          : in  std_logic;
  ctrl_int     : in  std_logic;
  out_b        : out std_logic;
end sys2;
```

## 4b
```vhdl
entity syss is
port (
  input_w      : in  std_logic;
  a_data       : in  std_logic_vector(7 downto 0);
  b_data       : in  std_logic_vector(7 downto 0);
  clk          : in  std_logic;
  dat_4        : out std_logic_vector(7 downto 0);
  dat_5        : out std_logic_vector(2 downto 0);
end sys2;
```

## 5a
![](Chapter3/5a.jpg)

## 5b
![](Chapter3/5b.jpg)

## 6a
Missing `;` on fourth line.

## 6b
Missing `)` on fifth line.
