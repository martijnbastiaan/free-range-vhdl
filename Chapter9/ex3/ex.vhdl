-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;

-- entity
entity ex is
  port
    ( A, B,  C   : in  std_logic
    ; F          : out std_logic
    );
end ex;

-- 3a)
architecture ex_a of ex is
  signal notA, notB, notC : std_logic;
  signal F0, F1 : std_logic;

  component andd
    port
      ( A, B : in std_logic
      ; O    : out std_logic );
  end component;

  component orr
    port
      ( A, B : in std_logic
      ; O    : out std_logic );
  end component;

  component nott
    port
      ( A : in std_logic
      ; O : out std_logic );
  end component;

begin
  not1A : nott port map (A, notA);
  not1B : nott port map (B, notB);
  not1C : nott port map (C, notC);

  and1 : andd port map (A, notB, F0);
  and2 : andd port map (notA, notC, F1);

  or1 : orr port map (F0, F1, F);

end ex_a;

-- 3b)
architecture ex_b of ex is
  signal F0 : std_logic_vector(7 downto 0);

  component threeEightDec
    port
      ( A, B, C : in std_logic
      ; O    : out std_logic_vector(7 downto 0) );
  end component;

  component orr4
    port
      ( A, B, C, D : in std_logic
      ; O          : out std_logic );
  end component;

begin
  lbl0 : threeEightDec port map (A, B, C, F0);
  lbl1 : orr4 port map (F0(0), F0(1), F0(3), F0(4), F);
end ex_b;

-- 3c)
architecture ex_c of ex is
  signal notA, notB, notC : std_logic;
  signal F0, F1 : std_logic;

  component andd
    port
      ( A, B : in std_logic
      ; O    : out std_logic );
  end component;

  component orr
    port
      ( A, B : in std_logic
      ; O    : out std_logic );
  end component;

  component nott
    port
      ( A : in std_logic
      ; O : out std_logic );
  end component;

begin
  not1B : nott port map (B, notB);
  not1C : nott port map (C, notC);

  and1 : andd port map (A, C, F0);
  and2 : andd port map (notC, notB, F1);

  or1 : orr port map (F0, F1, F);
end ex_c;

-- 3d)
architecture ex_d of ex is
  signal notA, notC : std_logic;
  signal F0, F1 : std_logic;

  component nandd
    port
      ( A, B : in std_logic
      ; O    : out std_logic );
  end component;

  component nott
    port
      ( A : in std_logic
      ; O : out std_logic );
  end component;

begin
  not1A : nott port map (A, notA);
  not1C : nott port map (C, notC);

  nand1 : nandd port map (notA, notC, F0);
  nand2 : nandd port map (A, B, F1);
  nand3 : nandd port map (F0, F1, F);
end ex_d;
