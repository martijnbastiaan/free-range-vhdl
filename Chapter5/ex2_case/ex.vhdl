library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A1,A2,B1,B2,D1 : in  std_logic
       ; O              : out std_logic
       );
end ex;

architecture ex_1 of ex is
  signal AABB : std_logic_vector(3 downto 0);
begin
  AABB <= A1 & A2 & B1 & B2;

  proc1: process(AABB)
  begin
    case AABB is
      when "11--" => O <= '1';
      when "--1-" => O <= '1';
      when "---1" => O <= '1';
      when others => O <= '0';
    end case;
  end process proc1;
end ex_1;
