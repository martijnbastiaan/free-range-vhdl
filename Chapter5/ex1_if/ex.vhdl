library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B : in  std_logic
       ; O   : out std_logic
       );
end ex;

architecture ex_1 of ex is
begin
  proc1: process(A, B)
  begin
    if    (A = '0' and B = '1') then O <= '1';
    elsif (A = '1')             then O <= '1';
    elsif (A = '1' and B = '0') then O <= '1';
    else                             O <= '0';
    end if;
  end process proc1;
end ex_1;
