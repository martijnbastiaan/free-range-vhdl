library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( I : in  std_logic_vector(7 downto 0)
       ; O : out std_logic
       );
end ex;

architecture ex_1 of ex is
begin
  proc1: process(I)
  begin
    case I is
      when "11111111" => O <= '0';
      when others     => O <= '1';
    end case;
  end process proc1;
end ex_1;
