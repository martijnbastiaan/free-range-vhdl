library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A,B : in  std_logic
       ; O   : out std_logic
       );
end ex;

architecture ex_1 of ex is
  signal AB :std_logic_vector(1 downto 0);
begin
  AB <= A & B;

  proc1: process(AB)
  begin
    case AB is
      when "01"   => O <= '1';
      when "1-"   => O <= '1';
      when "10"   => O <= '1';
      when others => O <= '0';
    end case;
  end process proc1;
end ex_1;
