library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( D0, D1, D2, D3, D4, D5, D6, D7 : in  std_logic
       ; SEL                            : in  std_logic_vector(2 downto 0)
       ; O                              : out std_logic
       );
end ex;

architecture ex_1 of ex is
begin
  proc1: process(SEL)
  begin
    case SEL is
      when "000"  => O <= D0;
      when "001"  => O <= D1;
      when "010"  => O <= D2;
      when "011"  => O <= D3;
      when "100"  => O <= D4;
      when "101"  => O <= D5;
      when "110"  => O <= D6;
      when "111"  => O <= D7;
      when others => O <= 'X';
    end case;
  end process proc1;
end ex_1;
