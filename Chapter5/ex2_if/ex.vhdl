library IEEE;
use IEEE.std_logic_1164.all;

entity ex is
  port ( A1,A2,B1,B2,D1 : in  std_logic
       ; O              : out std_logic
       );
end ex;

architecture ex_1 of ex is
begin
  proc1: process(A1,A2,B1,B2)
  begin
    if    (A1 = '1' and A2 = '1') then O <= '1';
    elsif (B1 = '1')              then O <= '1';
    elsif (B2 = '1')              then O <= '1';
    else                               O <= '0';
    end if;
  end process proc1;
end ex_1;
