# 4.8

## 1a
[solution](Chapter4/ex1a/ex.vhdl)

## 1b
[solution](Chapter4/ex1b/ex.vhdl)

## 1c
[solution](Chapter4/ex1c/ex.vhdl)

## 1d
[solution](Chapter4/ex1d/ex.vhdl)

## 1e
[solution](Chapter4/ex1e/ex.vhdl)

## 1f
[solution](Chapter4/ex1f/ex.vhdl)

## 2a
[cond solution](Chapter4/ex2a_cond/ex.vhdl)
[select solution](Chapter4/ex2a_select/ex.vhdl)

## 2bcd
busy work

## 3
[conc solution](Chapter4/ex3_conc/ex.vhdl)
[cond solution](Chapter4/ex3_cond/ex.vhdl)
[select solution](Chapter4/ex3_select/ex.vhdl)

## 4
[conc solution](Chapter4/ex4_conc/ex.vhdl)
[cond solution](Chapter4/ex4_cond/ex.vhdl)
[select solution](Chapter4/ex4_select/ex.vhdl)

## 5
[cond solution](Chapter4/ex5_cond/ex.vhdl)
[select solution](Chapter4/ex5_select/ex.vhdl)

## 6
[cond solution](Chapter4/ex6_cond/ex.vhdl)
[select solution](Chapter4/ex6_select/ex.vhdl)

## 7
busy work
