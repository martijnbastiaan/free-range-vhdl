# 5.7

## 1a
[case solution](Chapter5/ex1_case/ex.vhdl)
[if solution](Chapter5/ex1_if/ex.vhdl)

## 1bcde
busy work

## 2
[case solution](Chapter5/ex2_case/ex.vhdl)
[if solution](Chapter5/ex2_if/ex.vhdl)

## 3
busy work

## 4
[solution](Chapter5/ex4/ex.vhdl)

## 5
[solution](Chapter5/ex5/ex.vhdl)

## 6
[solution](Chapter5/ex6/ex.vhdl)

## 7
[solution](Chapter5/ex7/ex.vhdl)
